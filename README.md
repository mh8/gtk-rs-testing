# GTK-rs Sandbox Environment

A sandbox environment for developing GUI applications in Rust with GTK-rs.

## Current state

Basic


## Branches

**This branch :** Master

Exploring traditional building style (i.e. without .glade src) to explore some deeper connection with data.

**Other branches :**
- glade : Forked from earlier works on master - A working example of decent UI elements and minimal interaction

## More :

- Find out how to manage icon of application
- Delve into relationship GUI-frontend <> APP-backend
