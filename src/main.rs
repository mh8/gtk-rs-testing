//! #Testing GTK Code
// Sandbox Environment
// mh8 - November 2019

#![cfg_attr(not(feature = "gtk_3_10"), allow(unused_variables, unused_mut))]

extern crate gdk;
extern crate gio;
extern crate glib;
extern crate gtk;

use gio::prelude::*;
use gtk::prelude::*;
use gtk::{
    ApplicationWindow, Builder, Button, Dialog, Entry, Label, MessageDialog, PackType,
    ResponseType, Separator,
};

use std::env::args;
use std::str::FromStr;

// make moving clones into closures more convenient
macro_rules! clone {
    (@param _) => ( _ );
    (@param $x:ident) => ( $x );
    ($($n:ident),+ => move || $body:expr) => (
        {
            $( let $n = $n.clone(); )+
                move || $body
        }
    );
    ($($n:ident),+ => move |$($p:tt),+| $body:expr) => (
        {
            $( let $n = $n.clone(); )+
                move |$(clone!(@param $p),)+| $body
        }
    );
}

// upgrade weak reference or return
#[macro_export]
macro_rules! upgrade_weak {
    ($x:ident, $r:expr) => {{
        match $x.upgrade() {
            Some(o) => o,
            None => return $r,
        }
    }};
    ($x:ident) => {
        upgrade_weak!($x, ())
    };
}

fn create_sub_window(
    application: &gtk::Application,
    title: &str,
    content: &str
){
    let window = gtk::Window::new(gtk::WindowType::Toplevel);

    application.add_window(&window);

    let label = Label::new(Some(&content));

    let layout = gtk::Box::new(gtk::Orientation::Vertical, 5);
    layout.add(&label);
    layout.set_child_padding(&label, 10);

    window.set_title(title);
    window.add(&layout);
    window.set_default_size(400, 200);

    window.show_all();

}

fn build_ui(application: &gtk::Application) {
    /*Additional data - Back-end Program data
     */

    let mut number: isize = 0;
    let mut content = String::from("This is a string coded into the program");

    /*
    End Back-end data
    */

    let window = gtk::ApplicationWindowBuilder::new()
        .application(application)
        .title("GTK Testing Sandbox")
        .border_width(10)
        .window_position(gtk::WindowPosition::Center)
        .default_width(460)
        .default_height(380)
        .build();

    let menu_button: Button = Button::new_with_label("Menu");
    let separator: Separator = gtk::Separator::new(gtk::Orientation::Horizontal);
    let intro_label = Label::new(Some("Welcome to the GTK-rs Sandbox"));
    let entry: Entry = gtk::Entry::new();
    let counter_label: Label = Label::new(Some("0"));
    let ctr_box = gtk::Box::new(gtk::Orientation::Horizontal, 10);
    let plus_button: Button = Button::new_with_label("+");
    let minus_button: Button = Button::new_with_label("-");
    let create_window_button: Button = Button::new_with_label("Create a window");

    //Create +/- button box
    ctr_box.add(&minus_button);
    ctr_box.set_child_expand(&minus_button, true);
    ctr_box.add(&plus_button);
    ctr_box.set_child_expand(&plus_button, true);

    let vbox = gtk::Box::new(gtk::Orientation::Vertical, 0);
    vbox.add(&menu_button);
    vbox.set_child_expand(&menu_button, false);
    vbox.set_child_fill(&menu_button, false);
    vbox.set_child_padding(&menu_button, 8);
    vbox.set_child_pack_type(&menu_button, PackType::Start);

    vbox.add(&intro_label);
    vbox.set_child_padding(&intro_label, 8);

    vbox.add(&entry);
    vbox.set_child_padding(&entry, 8);

    vbox.add(&separator);
    vbox.set_child_padding(&separator, 8);

    vbox.add(&counter_label);
    vbox.set_child_padding(&counter_label, 8);

    vbox.add(&ctr_box);
    vbox.set_child_padding(&ctr_box, 8);

    vbox.add(&create_window_button);
    vbox.set_child_padding(&create_window_button, 8);

    window.add(&vbox);

    //Create weak
    let window_weak = window.downgrade();
    let entry_weak = entry.downgrade();
    let application_weak = application.downgrade();

    // MENU BUTTON ACTION AREA
    menu_button.connect_clicked(clone!(window_weak, entry_weak => move |_| {
        let window = upgrade_weak!(window_weak);
        let entry = upgrade_weak!(entry_weak);

        let dialog = Dialog::new_with_buttons(Some("Menu sub-Window"),
                                          Some(&window),
                                          gtk::DialogFlags::MODAL,
                                          &[("No", ResponseType::No),
                                            ("Yes", ResponseType::Yes),
                                            ("Other", ResponseType::Other(0))]);

        let ret = dialog.run();
        dialog.destroy();
        entry.set_text(&format!("Clicked {}", ret));
    }));

    // PLUS & MINUS BUTTONS ACTION AREA
    minus_button.connect_clicked(clone!(counter_label => move |_| {
        let nb = counter_label.get_text()
            .and_then(|s| isize::from_str(&s).ok())
            .unwrap_or(0);
            counter_label.set_text(&format!("{}", nb - 1));
    }));
    plus_button.connect_clicked(clone!(counter_label => move |_| {
        let nb = counter_label.get_text()
            .and_then(|s| isize::from_str(&s).ok())
            .unwrap_or(0);
        counter_label.set_text(&format!("{}", nb + 1));
    }));

    // CREATE WINDOW BUTTON ACTION AREA
    create_window_button.connect_clicked(clone!(window_weak => move |_| {
        let application = upgrade_weak!(application_weak);
        create_sub_window(&application, "Window title", &content);
    }));


    //Display Key-pressed events in terminal
    //This will not work if your focus is in a sub-window
    window.connect_key_press_event(clone!(entry_weak => move |_, key| {
        let entry = upgrade_weak!(entry_weak, Inhibit(false));

        let keyval = key.get_keyval();
        let keystate = key.get_state();

        println!("key pressed: {} / {:?}", keyval, keystate);
        println!("text: {}", entry.get_text().expect("Couldn't get text from entry"));

        if keystate.intersects(gdk::ModifierType::CONTROL_MASK) {
            println!("You pressed Ctrl!");
        }

        Inhibit(false)
    }));

    window.show_all();
}

fn main() {
    let application = gtk::Application::new(
        Some("fr.mh8.gtk-rs-testing"),
        gio::ApplicationFlags::empty(),
    )
    .expect("Initialization failed...");

    application.connect_activate(|app| {
        build_ui(app);
    });

    application.run(&args().collect::<Vec<_>>());
}
